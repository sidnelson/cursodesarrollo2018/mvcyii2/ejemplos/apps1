DROP DATABASE IF EXISTS apps1;
CREATE DATABASE apps1;
USE apps1;

DROP TABLE IF EXISTS articulo;
CREATE TABLE articulo (
  id_articulo int(11) AUTO_INCREMENT,
  titulo varchar(255),
  texto text,
  PRIMARY KEY (id_articulo)
  );

DROP TABLE IF EXISTS foto;
CREATE TABLE foto(
  id_foto int AUTO_INCREMENT,
  articulo int(11),
  nombre varchar(255),
  alt varchar(255),
  PRIMARY KEY (id_foto),
  UNIQUE KEY (articulo,nombre),
  CONSTRAINT FKfotoarticulo 
  FOREIGN KEY(articulo) REFERENCES articulo(id_articulo)
  );

DROP TABLE IF EXISTS usuarios;
CREATE TABLE usuarios (
  id INT(11) NOT NULL AUTO_INCREMENT,
  usuario VARCHAR(255) DEFAULT NULL,
  password VARCHAR(255) DEFAULT NULL,
  PRIMARY KEY (id)
)

INSERT INTO usuarios VALUES
(1, 'nelson', 'nelson'),
(2, 'ruben', 'ruben');

